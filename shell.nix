let
  rust_overlay = import (builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  pkgs = import <nixpkgs> { overlays = [ rust_overlay ]; };
in
with pkgs;
mkShell {
  buildInputs = [
    gdb
    picocom
    udev pkg-config
    openssl
    extism-cli
    wasmtime
    wasmedge
    openapi-generator-cli
    wayland
    libxkbcommon
    xorg.libX11

    (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
  ];
  NIXOS_OZONE_WL=1;
  LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.wayland}/lib:${pkgs.libxkbcommon}/lib";
  RUST_LOG = "info";
}
