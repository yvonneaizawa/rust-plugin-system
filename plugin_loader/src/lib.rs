use extism::*;
use log::{debug, error, info, log_enabled, warn, Level};
use plugin_shared::PluginMetadata;
use std::{
    collections::BTreeMap,
    fs::{self, File},
    io::Read,
};
use toml::*;
pub fn load_plugins() -> std::io::Result<Vec<LoadedPlugin>> {
    let path = "./plugins";

    // Read the contents of the directory
    let entries = fs::read_dir(path)?;

    // Collect file names ending with ".wasm" into a vector
    let wasm_plugins: Vec<LoadedPlugin> = entries
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                let file_name = e.file_name();
                let file_name = file_name.to_string_lossy().into_owned();
                if file_name.ends_with(".wasm") {
                    let tmp_manifest = Manifest::new([Wasm::file(e.path())]);
                    let mut tmp_plugin = Plugin::new(&tmp_manifest, [], false).unwrap();
                    let plugin_metadata = tmp_plugin
                        .call::<(), PluginMetadata>("plugin_metadata", ())
                        .unwrap();
                    let mut config = create_config_map(&plugin_metadata);
                    let loaded_plugin = match config {
                        Ok(cfg) => {
                            let manifest = Manifest::new([Wasm::file(e.path())].clone())
                                .with_config(cfg.into_iter())
                                .with_allowed_hosts(
                                    plugin_metadata.permissions.networkurls.iter().map(|s| s.clone()),
                                );
                            let plugin = Plugin::new(&manifest, [], false).unwrap();

                            Some(LoadedPlugin {
                                wasm: Wasm::file(e.path()),
                                plugin: plugin,
                            })
                        }
                        Err(e) => {
                            error!("{}", e);
                            None
                        }
                    };
                    loaded_plugin
                } else {
                    None
                }
            })
        })
        .collect();

    Ok(wasm_plugins)
}
pub fn create_config_map(plugin_config: &PluginMetadata) -> Result<BTreeMap<String, String>, Error> {
    let mut conf_file = File::open("config/test.toml")?;
    let mut contents = String::new();
    conf_file.read_to_string(&mut contents)?;
    debug!("{contents}");
    let config_toml = contents.parse::<Table>().unwrap();
    for (key,val) in &config_toml {
        debug!("test: {},{}",key,val);
    }
    let config_options = &plugin_config.permissions.config_file;
    let mut map = BTreeMap::new();
    for option in config_options {
        match config_toml.contains_key(&option.name) {
            true => match &config_toml[&option.name] {
                Value::Integer(val) => {
                    println!("{} is an integer with the value: {}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
                Value::Boolean(val) => {
                    println!("{} is a boolean with the value: {}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
                Value::String(val) => {
                    println!("{} is a string with the value: {}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
                Value::Float(val) => {
                    println!("{} is a float with the value: {}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
                Value::Datetime(val) => {
                    println!("{} is a datetime with the value: {}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
                Value::Array(val) => {
                    println!("{} is an array with the value: {:?}", option.name, val);
                    // i have no idea how to yeet this to string
                    warn!("Option NOT added to config. idk how to convert it to a string")
                }
                Value::Table(val) => {
                    println!("{} is a table with the value: {:?}", option.name, val);
                    map.insert(option.name.clone(), val.to_string());
                }
            },
            false => match &option.required {
                true => {
                    warn!("Required Option ({}) not found", option.name);
                    return Err(Error::msg(format!(
                        "Option {} is missing but required by the plugin",
                        option.name
                    )));
                }
                false => {
                    warn!("Optional Option ({}) not found", option.name);
                    warn!("Defaulting to default value");
                    match &option.default {
                        Some(value) => {
                            println!("Using default value for {}: {}",option.name, value);
                            map.insert(option.name.clone(), value.to_string());
                        }
                        None => {
                            warn!("No default value provided");
                            warn!("Skipping option");
                        }
                    }
                },
            },
        }
    }
    // let map = BTreeMap::new();

    Ok(map)
}
pub struct LoadedPlugin {
    pub wasm: Wasm,
    pub plugin: Plugin,
}
pub trait LoadedPluginTrait {
    fn get_metadata(self) -> PluginMetadata;
    fn call<'a, 'b, T: ToBytes<'a>, U: FromBytes<'b>>(
        &'b mut self,
        name: impl AsRef<str>,
        input: T,
    ) -> Result<U, Error>;
    fn reload(&mut self, Manifest: Option<Manifest>) -> &mut Self;
}
impl LoadedPluginTrait for LoadedPlugin {
    fn get_metadata(mut self) -> PluginMetadata {
        let metadata = self
            .plugin
            .call::<(), PluginMetadata>("plugin_metadata", ())
            .unwrap();
        metadata
    }
    //plugin.call::<(), PluginMetadata>("plugin_metadata", ());
    //lamo instead of having to run plugin.plugin.call i just did some magic. it works
    fn call<'a, 'b, T: ToBytes<'a>, U: FromBytes<'b>>(
        &'b mut self,
        name: impl AsRef<str>,
        input: T,
    ) -> Result<U, Error> {
        let result = self.plugin.call(name, input)?;
        Ok(result)
    }
    fn reload(&mut self, mut manifest: Option<Manifest>) -> &mut Self {
        let wasm = &self.wasm;
        match manifest {
            Some(mut m) => {
                m.wasm = [wasm.clone()].to_vec();
                let new_plugin = Plugin::new(&m, [], false).unwrap();
                *self = LoadedPlugin {
                    wasm: wasm.clone(),
                    plugin: new_plugin,
                };
                self
            }
            None => {
                let new_plugin = Plugin::new(Manifest::new([wasm.clone()]), [], false).unwrap();
                *self = LoadedPlugin {
                    wasm: wasm.clone(),
                    plugin: new_plugin,
                };
                self
            }
        }
    }
}

pub struct LoadedPlugins {
    plugins: Vec<Plugin>,
}
