use extism::*;
use log::{debug, error, info, log_enabled, Level};
use plugin_loader::*;
use plugin_shared::*;
fn main() {
    env_logger::init();
    info!("Starting");
    let result = plugin_loader::load_plugins();
    match result {
        Ok(mut plugins) => {
            for plugin in plugins.iter_mut() {
                let res = plugin.call::<(), String>("config_test", ());
                //lamo instead of having to run plugin.plugin.call i just did some magic. it works
                println!("yay: {:?}", res);
            }
        }
        Err(_) => panic!("Failed to load plugins"),
    }
}
