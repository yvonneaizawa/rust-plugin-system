use extism_pdk::*;
use serde::{Deserialize, Serialize};
use serde_json::*;
#[derive(Debug, Serialize, Deserialize)]
pub struct ConfigOptions {
    pub name: String,
    pub description: String,
    pub required: bool,
    pub default: Option<String>
}
#[derive(Debug, Serialize, Deserialize)]
pub struct PluginMetadata {
    pub uuid: String,
    pub version: String,
    pub name: String,
    pub permissions: PluginPermissions,
    pub nodes: Vec<NodeInfo>,
}
#[derive(Debug, Serialize, Deserialize)]
pub struct PluginPermissions {
    pub config_file : Vec<ConfigOptions>,
    pub networkurls : Vec<String>,
}
impl<'a> ToBytes<'a> for PluginMetadata {
    fn to_bytes(&self) -> std::prelude::v1::Result<Self::Bytes, extism_pdk::Error> {
        //convert to json string
        //then encode it in Vec<u8>
        let json_string = serde_json::to_string(self)?;
        Ok(json_string.as_bytes().to_vec())
    }
    type Bytes = Vec<u8>;
}

impl<'a> FromBytes<'a> for PluginMetadata {
    fn from_bytes(data: &'a [u8]) -> std::prelude::v1::Result<Self, extism_pdk::Error> {
        //convert from Vec<u8> to string
        //convert from string to PluginMetadata using serde_json
        let json_string = String::from_utf8(data.to_vec()).unwrap();
        let plugin_metadata: PluginMetadata = from_str(&json_string).unwrap();
        Ok(plugin_metadata)
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct NodeInfo {
    pub id: u64,
}
