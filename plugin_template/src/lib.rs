use extism_pdk::{config::get, LogLevel, *};
use plugin_shared::*;
use serde::*;
use std::collections::BTreeMap;
#[plugin_fn]
pub fn plugin_metadata() -> FnResult<PluginMetadata> {
    Ok(get_metadata())
}
#[plugin_fn]
pub fn config_test() -> FnResult<String> {
    match config::get("test").unwrap() {
        None => {
            return Ok(format!("None"));
        }
        Some(v) => {
            return Ok(format!("The value of test is: {}", v));
        }
    }
}
pub fn get_metadata() -> PluginMetadata {
    extism_pdk::log!(LogLevel::Debug, "Metadata was requested");
    let urls = vec!["ifconfig.co".to_string(), "google.com".to_string()];
    let metadata = PluginMetadata {
        name: "example-plugin".to_string(),
        permissions: PluginPermissions{
            config_file: vec![ConfigOptions {
                name: "test".to_string(),
                description: "for testing".to_string(),
                required: false,
                default: Some("I am a default value".to_string()),
            }],
            networkurls: urls
        },
        uuid: "dece1f76-21ef-474d-bcde-a33ef6de453a".to_string(),
        version: "0.0.1".to_string(),

        nodes: vec![],
    };
    metadata
}
#[plugin_fn]
pub fn ping() -> FnResult<String> {
    extism_pdk::log!(LogLevel::Debug, "Ping was requested");
    let name = get_metadata().name;
    let req = HttpRequest {
        url: "https://ifconfig.co/json".to_string(),
        headers: BTreeMap::new(),
        method: Some("GET".to_string()),
    };
    let res = http::request::<()>(&req, None)?;
    Ok(format!(
        "pinged ifconfig.co status code: {:?}",
        res.json::<IfconfigResponse>()?.ip
    ))
}
#[derive(Serialize, Deserialize)]
pub struct IfconfigResponse {
    ip: String,
    ip_decimal: u32,
}
